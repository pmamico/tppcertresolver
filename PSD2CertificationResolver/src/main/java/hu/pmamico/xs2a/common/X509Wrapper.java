package hu.pmamico.xs2a.common;

import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.misc.VerisignCzagExtension;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

public class X509Wrapper {
    private X509Certificate cert;
    static final String PSD2_ROLE_OID = "0.4.0.19495.1.1";

    public X509Wrapper(X509Certificate cert){
        this.cert = cert;
    }

    public String format() throws Exception {
        StringBuffer buf = new StringBuffer();
        String nl = System.lineSeparator();

        buf.append("  [0]         Version: ").append(this.cert.getVersion()).append(nl);
        buf.append("         SerialNumber: ").append(this.cert.getSerialNumber()).append(nl);
        buf.append("             IssuerDN: ").append(this.cert.getIssuerDN().toString()).append(nl);
        buf.append("           Start Date: ").append(this.cert.getNotBefore()).append(nl);
        buf.append("           Final Date: ").append(this.cert.getNotAfter()).append(nl);
        buf.append("            SubjectDN: ").append(this.cert.getSubjectDN().toString()).append(nl);
        buf.append("           Public Key: ").append(this.cert.getPublicKey()).append(nl);
        buf.append("  Signature Algorithm: ").append(this.cert.getSigAlgName()).append(nl);

        byte[] sig = this.cert.getSignature();

        buf.append("            Signature: ").append(new String(Hex.encode(sig, 0, 20))).append(nl);
        for (int i = 20; i < sig.length; i += 20) {
            if (i < sig.length - 20) {
                buf.append("                       ").append(new String(Hex.encode(sig, i, 20))).append(nl);
            } else {
                buf.append("                       ").append(new String(Hex.encode(sig, i, sig.length - i))).append(nl);
            }
        }

        TBSCertificateStructure tbs = TBSCertificateStructure.getInstance(ASN1Sequence.fromByteArray(cert.getTBSCertificate()));
        X509Extensions extensions = tbs.getExtensions();

        if (extensions != null) {
            Enumeration e = extensions.oids();

            if (e.hasMoreElements()) {
                buf.append("       Extensions: \n");
            }

            while (e.hasMoreElements()) {
                ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) e.nextElement();
                X509Extension ext = extensions.getExtension(oid);

                if (ext.getValue() != null) {
                    byte[] octs = ext.getValue().getOctets();
                    ASN1InputStream dIn = new ASN1InputStream(octs);
                    buf.append("                       critical(").append(ext.isCritical()).append(") ");
                    try {
                        if (oid.equals(Extension.basicConstraints)) {
                            buf.append(BasicConstraints.getInstance((ASN1Sequence) dIn.readObject())).append(nl);
                        } else if (oid.equals(Extension.keyUsage)) {
                            buf.append(KeyUsage.getInstance((DERBitString) dIn.readObject())).append(nl);
                        } else if (oid.equals(MiscObjectIdentifiers.netscapeCertType)) {
                            buf.append(new NetscapeCertType((DERBitString) dIn.readObject())).append(nl);
                        } else if (oid.equals(MiscObjectIdentifiers.netscapeRevocationURL)) {
                            buf.append(new NetscapeRevocationURL((DERIA5String) dIn.readObject())).append(nl);
                        } else if (oid.equals(MiscObjectIdentifiers.verisignCzagExtension)) {
                            buf.append(new VerisignCzagExtension((DERIA5String) dIn.readObject())).append(nl);
                            //*********************************************************
                            // *** HERE: code to handle TNAuthorizationList ***
                            //*********************************************************
                        //} else if (oid.equals(TNAuthorizationList.TN_AUTH_LIST_OID))
                        //     buf.append(TNAuthorizationList.getInstance((ASN1Sequence) dIn.readObject())).append(nl);
                        } else {
                            buf.append(oid.getId());
                            buf.append(" value = ").append(ASN1Dump.dumpAsString(dIn.readObject())).append(nl);
                        }
                    } catch (Exception ex) {
                        buf.append(oid.getId());
                        buf.append(" value = ").append("*****").append(nl);
                    }
                } else {
                    buf.append(nl);
                }
            }
        }

        return buf.toString();
    }

    public String getSignature(){
        StringBuffer buf = new StringBuffer();
        byte[] sig = this.cert.getSignature();
        String nl = System.lineSeparator();

        buf.append("Signature: ").append(new String(Hex.encode(sig, 0, 20))).append(nl);
        for (int i = 20; i < sig.length; i += 20) {
            if (i < sig.length - 20) {
                buf.append(new String(Hex.encode(sig, i, 20))).append(nl);
            } else {
                buf.append(new String(Hex.encode(sig, i, sig.length - i))).append(nl);
            }
        }
        return buf.toString();
    }

    public String getRole() throws CertificateEncodingException, IOException {
        String ret = "";
        String nl = System.lineSeparator();
        TBSCertificateStructure tbs = TBSCertificateStructure.getInstance(ASN1Sequence.fromByteArray(cert.getTBSCertificate()));
        X509Extensions extensions = tbs.getExtensions();

        if (extensions != null) {
            Enumeration e = extensions.oids();

            while (e.hasMoreElements()) {
                ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) e.nextElement();
                X509Extension ext = extensions.getExtension(oid);

                if (ext.getValue() != null) {
                    byte[] octs = ext.getValue().getOctets();
                    ASN1InputStream dIn = new ASN1InputStream(octs);
                    try {
                        if (oid.equals(Extension.basicConstraints)) {
                            System.out.println(BasicConstraints.getInstance((ASN1Sequence) dIn.readObject()));
                        } else if (oid.equals(Extension.keyUsage)) {
                            System.out.println(KeyUsage.getInstance((DERBitString) dIn.readObject()));
                        } else if (oid.equals(MiscObjectIdentifiers.netscapeCertType)) {
                            System.out.println(new NetscapeCertType((DERBitString) dIn.readObject()));
                        } else if (oid.equals(MiscObjectIdentifiers.netscapeRevocationURL)) {
                            System.out.println(new NetscapeRevocationURL((DERIA5String) dIn.readObject()));
                        } else if (oid.equals(MiscObjectIdentifiers.verisignCzagExtension)) {
                            System.out.println(new VerisignCzagExtension((DERIA5String) dIn.readObject()));
                            //*********************************************************
                            // *** HERE: code to handle TNAuthorizationList ***
                            //*********************************************************
                            //} else if (oid.equals(TNAuthorizationList.TN_AUTH_LIST_OID))
                            //     buf.append(TNAuthorizationList.getInstance((ASN1Sequence) dIn.readObject())).append(nl);
                        } else {
                            System.out.println(oid.getId());
                            System.out.println(" value = "+ASN1Dump.dumpAsString(dIn.readObject()));
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {

                }
            }
        }
        return ret;
    }



    @Override
    public String toString(){
        String ret = null;
        try {
            ret = format();
        }catch (Exception e){
            ret = "DEFAULT TOSTRING"+System.lineSeparator()+cert.toString();
        }
        return ret;
    }
}
