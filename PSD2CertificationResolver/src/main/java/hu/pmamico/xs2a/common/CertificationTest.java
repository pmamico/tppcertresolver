package hu.pmamico.xs2a.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.nimbusds.jose.util.X509CertUtils;

public class CertificationTest {


    public static void main(String args[]) {
        System.out.println("CertificationTest start...");
        CertificationTest main = new CertificationTest();
        File file = main.getFileFromResources("tpp.cert");

        String encodedCert = null;
        try {
            encodedCert = loadStringFromFile(file);
            System.out.println("encoded cert:"+System.lineSeparator()+encodedCert);
            X509Certificate cert = X509CertUtils.parse(encodedCert);
            X509Wrapper wrapper = new X509Wrapper(cert);
            System.out.println("----------------------------");
            System.out.println(wrapper.getRole());

        } catch (Exception e) {
            e.printStackTrace();
        }




        System.out.println("CertificationTest end...");
    }


    // get file from classpath, resources folder
    private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }

    private static void printFile(File file) throws IOException {
        if (file == null) return;
        BufferedReader br = null;
        try {
            FileReader reader = new FileReader(file);
            br = new BufferedReader(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
    }

    private static String loadStringFromFile(File file)throws IOException {
        StringBuilder sb = new StringBuilder();

        if (file == null) return null;
        BufferedReader br = null;
        try {
            FileReader reader = new FileReader(file);
            br = new BufferedReader(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
        }

        return sb.toString();
    }


    private static Map<String, Object> certMapping(X509Certificate cert){
        Map<String, Object> map = new TreeMap<>();

        map.put("SigAlgName", cert.getSigAlgName());
        map.put("Signature", cert.getSignature());
        map.put("BasicConstraints", cert.getBasicConstraints());
        map.put("IssuerX500Principal", cert.getIssuerX500Principal());

        try {
            map.put("TBSCertificate", cert.getTBSCertificate());
        } catch (CertificateEncodingException e) {
            map.put("ALERT TBSCertificate", e.getCause());
        }

        return map;
    }

    private static void log(Map<String, Object> map){
        for(Map.Entry<String, Object> entry : map.entrySet()){
            System.out.println("--------");
            System.out.println(entry.getKey());
            System.out.println("--------");
            System.out.println(entry.getValue());
            System.out.println("....................................");
        }
    }
}
